package main

import (
	"app/internal/admin"
	"app/internal/authorization"
	"app/internal/chat"
	"app/internal/item"
	"app/internal/product"
	"app/internal/variant"
	"app/pkg/cors"
	"app/pkg/db/client"
	"app/pkg/db/s3"
	"app/pkg/security/middleware"
	"app/pkg/security/tokenManager"
	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	"log"
	"net/http"
)

func main() {

	err := godotenv.Load(".env")
	if err != nil {
		log.Print(err)
		return
	}

	gin.SetMode(gin.ReleaseMode)
	// TOOLS
	engine := gin.Default()

	TokenManager := tokenManager.NewTokenManager()
	MiddleWare := middleware.NewMiddleWare(TokenManager)
	s3Client := s3.NewS3Client()
	pgClient, err := client.GetDBClient(true)
	if err != nil {
		log.Fatal(err)
		return
	}

	api := engine.Group("/api")
	AuthGroupAPI := api.Group("/auth")
	AdminGroupAPI := api.Group("/admin")
	ProductGroupAPI := api.Group("/products")

	product.RegisterProductService(pgClient, MiddleWare, s3Client, ProductGroupAPI)
	admin.RegisterAdminService(pgClient, MiddleWare, AdminGroupAPI)
	authorization.RegisterAuthService(pgClient, TokenManager, AuthGroupAPI)
	variant.RegisterVariantService(pgClient, *MiddleWare, ProductGroupAPI)
	item.RegisterItemService(*MiddleWare, ProductGroupAPI, pgClient)
	chat.InitChatService(*MiddleWare, api.Group("/chat"))
	handler := cors.GetCORSHandler(engine)

	err = http.ListenAndServe(":8080", handler)
	if err != nil {
		log.Fatal(err)
		return
	}
}
