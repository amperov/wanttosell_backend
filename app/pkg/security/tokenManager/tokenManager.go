package tokenManager

import (
	"errors"
	"github.com/golang-jwt/jwt/v5"

	"github.com/spf13/viper"
	"log"
	"time"
)

type TokenManager struct {
}

var singingKey = viper.GetString("key.jwt")

type TokenClaims struct {
	*jwt.RegisteredClaims
	UserId   int    `json:"user_id"`
	UserRole string `json:"user_role"`
}

func NewTokenManager() TokenManager {
	return TokenManager{}
}

func (t *TokenManager) getToken(ID int, Role string, exp *jwt.NumericDate, iss *jwt.NumericDate) (string, error) {
	Claims := jwt.NewWithClaims(jwt.SigningMethodHS256, &TokenClaims{
		&jwt.RegisteredClaims{
			IssuedAt:  iss,
			ExpiresAt: exp,
		},
		ID,
		Role,
	})

	Token, err := Claims.SignedString([]byte(singingKey))
	if err != nil {
		return "", err
	}
	return Token, nil
}

func (t *TokenManager) GenerateTokens(id int, role string) (string, string, error) {
	if id == 0 {
		return "", "", errors.New("error: id = 0; unauthorized")
	}

	issuedAt := jwt.NewNumericDate(time.Now())
	expiresAccess := jwt.NewNumericDate(time.Now().Add(60 * time.Minute))
	expiresRefresh := jwt.NewNumericDate(time.Now().Add(60 * 24 * 30 * time.Minute))

	accessToken, err := t.getToken(id, role, expiresAccess, issuedAt)
	if err != nil {
		return "", "", err
	}

	refreshToken, err := t.getToken(id, role, expiresRefresh, issuedAt)
	if err != nil {
		return "", "", err
	}

	return accessToken, refreshToken, nil
}

func (t *TokenManager) ValidateToken(accessToken string) (int, string, error) {

	aToken, err := jwt.ParseWithClaims(accessToken, &TokenClaims{}, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, errors.New("invalid access-token")
		}
		return []byte(singingKey), nil
	})
	if err != nil {
		log.Print(err.Error())
	}

	claims, ok := aToken.Claims.(*TokenClaims)
	if !ok || !aToken.Valid {
		return 0, "", errors.New("invalid token")
	}

	return claims.UserId, claims.UserRole, nil
}
