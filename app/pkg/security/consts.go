package security

var OnlyAdminAccess = []string{"admin"}
var SellerAdminAccess = []string{"admin", "seller"}
var AllAccess = []string{"admin", "seller", "user"}
