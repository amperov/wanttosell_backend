package middleware

import (
	"app/pkg/security/tokenManager"
	"fmt"
	"github.com/gin-gonic/gin"
	"strings"
)

type MiddleWare struct {
	tokenManager.TokenManager
}

func NewMiddleWare(tokenManager tokenManager.TokenManager) *MiddleWare {
	return &MiddleWare{TokenManager: tokenManager}
}

func (m *MiddleWare) Identity(next func(ctx *gin.Context)) func(ctx *gin.Context) {
	return func(ctx *gin.Context) {

		Bearer := ctx.Request.Header.Get("Authorization")
		if Bearer != "" {
			var Token = strings.Split(Bearer, " ")[1]
			UserID, UserRole, err := m.ValidateToken(Token)
			if err != nil {
				ctx.JSON(401, nil)
				return
			}
			ctx.Set("Authed", true)
			ctx.Set("UserID", UserID)
			ctx.Set("UserRole", UserRole)
			next(ctx)
		} else {
			ctx.Set("Authed", false)
		}
	}
}

func (m *MiddleWare) NeedRole(next func(ctx *gin.Context), OpenTo []string) func(ctx *gin.Context) {

	return func(ctx *gin.Context) {
		IsAuthed, _ := ctx.Get("Authed")
		if IsAuthed.(bool) {
			UserRole, exists := ctx.Get("UserRole")
			if exists {
				match := false
				for _, Role := range OpenTo {
					if UserRole.(string) == Role {
						match = true
					}
				}
				if match {
					next(ctx)
				} else {
					ctx.JSON(403, fmt.Sprintf("no access. Have access only %v", OpenTo))
				}
			}
		} else {
			ctx.JSON(401, nil)
		}
	}
}
