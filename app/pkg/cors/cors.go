package cors

import (
	"github.com/rs/cors"
	"net/http"
)

func GetCORSHandler(handler http.Handler) http.Handler {
	options := cors.Options{
		AllowedOrigins:         []string{"*"},
		AllowOriginFunc:        nil,
		AllowOriginRequestFunc: nil,
		AllowedMethods:         []string{"POST", "PATCH", "GET", "DELETE"},
		AllowedHeaders:         []string{"Access-Control-Allow-Origin", "Authorization", "Content-Type"},
		ExposedHeaders:         nil,
		MaxAge:                 0,
		AllowCredentials:       true,
		AllowPrivateNetwork:    false,
		OptionsPassthrough:     false,
		OptionsSuccessStatus:   0,
		Debug:                  false,
	}
	c := cors.New(options)

	CorsHandler := c.Handler(handler)
	return CorsHandler
}
