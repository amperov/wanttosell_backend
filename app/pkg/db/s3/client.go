package s3

import (
	"context"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"io"
	"os"
	"time"
)

const (
	vkCloudHotboxEndpoint = "https://hb.bizmrg.com"
	defaultRegion         = "us-east-1"
)

type Client struct {
	Bucket string
	conn   *s3.S3
}

func NewS3Client() *Client {
	sess, _ := session.NewSession()

	// Подключение к сервису S3
	svc := s3.New(sess, aws.NewConfig().WithEndpoint(vkCloudHotboxEndpoint).WithRegion(defaultRegion))
	return &Client{conn: svc, Bucket: os.Getenv("AWS_BUCKET")}
}

func (c *Client) SendFile(ctx context.Context, FileKey string, File io.ReadSeeker) error {

	_, err := c.conn.PutObject(&s3.PutObjectInput{
		Bucket: aws.String(c.Bucket),
		Key:    aws.String(FileKey),
		Body:   File,
	})
	if err != nil {
		return err
	}

	return nil
}

func (c *Client) GetFile(ctx context.Context, FileKey string) (string, error) {

	Object, _ := c.conn.GetObjectRequest(&s3.GetObjectInput{
		Bucket: aws.String(c.Bucket),
		Key:    aws.String(FileKey),
	})

	Link, err := Object.Presign(time.Minute * 30)
	if err != nil {
		return "", err
	}
	return Link, nil

}
