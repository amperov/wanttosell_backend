package client

import (
	"context"
	"errors"
	"fmt"
	"github.com/jackc/pgx/v5/pgxpool"
	"os"

	"log"
)

type PGConfig struct {
	Host     string
	Port     string
	Username string
	Password string
	DBName   string
}

func GetDBClient(isProd bool) (*pgxpool.Pool, error) {
	DBConfig, err := InitPGConfig(isProd)
	if err != nil {
		return nil, err
	}

	Client, err := GetPGClient(context.Background(), DBConfig)
	if err != nil {
		return nil, err
	}
	return Client, nil
}

func InitPGConfig(isProd bool) (*PGConfig, error) {
	var cfg PGConfig
	if isProd {
		cfg.DBName = os.Getenv("PROD_DB_NAME")
		cfg.Host = os.Getenv("PROD_DB_HOST")
		cfg.Username = os.Getenv("PROD_DB_USERNAME")
		cfg.Password = os.Getenv("PROD_DB_PASSWORD")
	}
	if !isProd {
		cfg.DBName = os.Getenv("DEV_DB_NAME")
		cfg.Host = os.Getenv("DEV_DB_HOST")
		cfg.Username = os.Getenv("DEV_DB_USERNAME")
		cfg.Password = os.Getenv("DEV_DB_PASSWORD")
		cfg.Port = os.Getenv("DEV_DB_PORT")
	}

	if cfg.Host == "" || cfg.Password == "" || cfg.Username == "" || cfg.DBName == "" {
		return nil, errors.New(fmt.Sprintf("error: value from config is null: %+v ", cfg))
	}
	return &cfg, nil
}

func GetPGClient(ctx context.Context, cfg *PGConfig) (*pgxpool.Pool, error) {
	dsn := fmt.Sprintf("postgresql://%s:%s@%s:%s/%s", cfg.Username, cfg.Password, cfg.Host, cfg.Port, cfg.DBName)

	pool, err := pgxpool.New(ctx, dsn)
	if err != nil {
		return nil, err
	}
	err = pool.Ping(ctx)
	if err != nil {
		log.Fatal(err)
		return nil, err
	}
	return pool, nil
}
