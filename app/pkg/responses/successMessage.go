package responses

type SuccessResponse struct {
	StatusMessage string `json:"status"`
}
