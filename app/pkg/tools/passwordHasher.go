package tools

import (
	"crypto/sha256"
	"encoding/hex"
)

func HashPassword(Password string) string {
	Sha := sha256.New()
	Sha.Write([]byte(Password))

	HashedPassword := hex.EncodeToString(Sha.Sum(nil))
	return HashedPassword
}
