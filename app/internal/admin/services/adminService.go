package services

import "context"

type RoleStorage interface {
	ChangeRole(ctx context.Context, UserID int, NewRole string) error
}
type AdminService struct {
	roleStorage RoleStorage
}

func NewAdminService(roleStorage RoleStorage) *AdminService {
	return &AdminService{roleStorage: roleStorage}
}

func (s *AdminService) GrantRole(ctx context.Context, UserID int, Role string) error {
	err := s.roleStorage.ChangeRole(ctx, UserID, Role)
	if err != nil {
		return err
	}
	return err
}

func (s *AdminService) TakeoffRole(ctx context.Context, UserID int) error {
	err := s.roleStorage.ChangeRole(ctx, UserID, "user")
	if err != nil {
		return err
	}

	return nil
}
