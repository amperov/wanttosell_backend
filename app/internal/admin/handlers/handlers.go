package handlers

import (
	responses2 "app/pkg/responses"
	"app/pkg/security"
	"app/pkg/security/middleware"
	"context"
	"fmt"
	"github.com/gin-gonic/gin"
	"strconv"
)

type AdminHandlers struct {
	mw           *middleware.MiddleWare
	AdminService AdminService
}

func NewAdminHandlers(mw *middleware.MiddleWare, adminService AdminService) *AdminHandlers {
	return &AdminHandlers{mw: mw, AdminService: adminService}
}

type AdminService interface {
	GrantRole(ctx context.Context, UserID int, Role string) error
	TakeoffRole(ctx context.Context, UserID int) error
}

func (h *AdminHandlers) Register(group *gin.RouterGroup) {
	group.POST("/role/takeoff", h.mw.Identity(h.mw.NeedRole(h.TakeOffRole, security.OnlyAdminAccess)))
	group.POST("/role/grant", h.mw.Identity(h.mw.NeedRole(h.GrantRole, security.OnlyAdminAccess)))
}

func (h *AdminHandlers) GrantRole(ctx *gin.Context) {
	Userid := ctx.Query("user_id")
	UserID, err := strconv.Atoi(Userid)
	if err != nil {
		return
	}

	NewRole := ctx.Query("role")

	err = h.AdminService.GrantRole(context.Background(), UserID, NewRole)
	if err != nil {
		ctx.JSON(402, responses2.ErrorResponse{ErrorMessage: err.Error()})
		return
	}

	ctx.JSON(200, responses2.SuccessResponse{
		StatusMessage: fmt.Sprintf("Role[%s] successfully granted to user[%d]", NewRole, UserID),
	})

}

func (h *AdminHandlers) TakeOffRole(ctx *gin.Context) {
	Userid, existsID := ctx.Params.Get("user_id")

	UserID, err := strconv.Atoi(Userid)
	if err != nil {
		return
	}

	if !existsID {
		ctx.JSON(402, responses2.ErrorResponse{
			ErrorMessage: "user_id not found in request",
		})
	}
	err = h.AdminService.TakeoffRole(context.Background(), UserID)
	if err != nil {
		ctx.JSON(402, responses2.ErrorResponse{ErrorMessage: err.Error()})
		return
	}

	ctx.JSON(200, responses2.SuccessResponse{
		StatusMessage: fmt.Sprintf("Role successfully took off from user[%d]", UserID),
	})
}
