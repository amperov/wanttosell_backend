package storages

import (
	"context"
	sq "github.com/Masterminds/squirrel"
	"github.com/jackc/pgx/v5/pgxpool"
)

const UserTable = "user_table"

type RoleStorage struct {
	pool *pgxpool.Pool
}

func NewRoleStorage(pool *pgxpool.Pool) *RoleStorage {
	return &RoleStorage{pool: pool}
}

func (r *RoleStorage) ChangeRole(ctx context.Context, UserID int, NewRole string) error {
	query, args, err := sq.Update(UserTable).Where(sq.Eq{"id": UserID}).PlaceholderFormat(sq.Dollar).Set("role", NewRole).ToSql()
	if err != nil {
		return err
	}
	_, err = r.pool.Exec(ctx, query, args...)
	if err != nil {
		return err
	}
	return nil

}
