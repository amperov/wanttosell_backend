package admin

import (
	"app/internal/admin/handlers"
	"app/internal/admin/services"
	"app/internal/admin/storages"
	"app/pkg/security/middleware"
	"github.com/gin-gonic/gin"
	"github.com/jackc/pgx/v5/pgxpool"
)

func RegisterAdminService(pool *pgxpool.Pool, MiddleWare *middleware.MiddleWare, group *gin.RouterGroup) {
	RoleStorage := storages.NewRoleStorage(pool)
	AdminService := services.NewAdminService(RoleStorage)
	adminHandlers := handlers.NewAdminHandlers(MiddleWare, AdminService)
	adminHandlers.Register(group)

}
