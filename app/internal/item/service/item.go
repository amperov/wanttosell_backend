package service

import (
	"app/internal/entities"
	"context"
	"fmt"
)

type ItemStorage interface {
	Create(ctx context.Context, data map[string]interface{}) error
	Update(ctx context.Context, updatedItem, ItemUUID string, VariantID int) error
	Delete(ctx context.Context, ItemUUID string, VariantID int) error
	GetAll(ctx context.Context, VariantID int) ([]entities.Item, error)
}

type VariantStorage interface {
	CheckExist(ctx context.Context, VariantID, ProductID int) bool
}

type ProductStorage interface {
	CheckExist(ctx context.Context, ProductID, UserID int) bool
}
type ItemService struct {
	ProductStorage ProductStorage
	ItemStorage    ItemStorage
	VariantStorage VariantStorage
}

func (i *ItemService) GetAll(ctx context.Context, VariantID, ProductID, UserID int) ([]entities.Item, error) {
	ProductExist := i.ProductStorage.CheckExist(ctx, ProductID, UserID)
	if ProductExist {
		VariantExist := i.VariantStorage.CheckExist(ctx, VariantID, ProductID)
		if VariantExist {
			Items, err := i.ItemStorage.GetAll(ctx, VariantID)
			if err != nil {
				return nil, err
			}
			return Items, nil
		}
	}
	return nil, fmt.Errorf("broken credentials")
}

func NewItemService(productStorage ProductStorage, itemStorage ItemStorage, variantStorage VariantStorage) *ItemService {
	return &ItemService{ProductStorage: productStorage, ItemStorage: itemStorage, VariantStorage: variantStorage}
}

func (i *ItemService) CreateOne(ctx context.Context, data map[string]interface{}) (string, error) {
	err := i.ItemStorage.Create(ctx, data)
	if err != nil {
		return "", err
	}
	return "success", nil
}

func (i *ItemService) UpdateOne(ctx context.Context, updatedItem, ItemUUID string, VariantID, ProductID, UserID int) error {
	ProductExist := i.ProductStorage.CheckExist(ctx, ProductID, UserID)
	if ProductExist {
		VariantExist := i.VariantStorage.CheckExist(ctx, VariantID, ProductID)
		if VariantExist {
			err := i.ItemStorage.Update(ctx, updatedItem, ItemUUID, VariantID)
			if err != nil {
				return err
			}
		}
	}
	return fmt.Errorf("broken credentials")
}

func (i *ItemService) DeleteOne(ctx context.Context, ItemUUID string, VariantID, ProductID, UserID int) error {
	ProductExist := i.ProductStorage.CheckExist(ctx, ProductID, UserID)
	if ProductExist {
		VariantExist := i.VariantStorage.CheckExist(ctx, VariantID, ProductID)
		if VariantExist {
			err := i.ItemStorage.Delete(ctx, ItemUUID, VariantID)
			if err != nil {
				return err
			}
			return err
		}
	}
	return fmt.Errorf("broken credentials")
}
