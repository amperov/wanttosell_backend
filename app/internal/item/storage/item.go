package storage

import (
	"app/internal/entities"
	"context"
	sq "github.com/Masterminds/squirrel"
	"github.com/jackc/pgx/v5/pgxpool"
)

var builder = sq.StatementBuilder.PlaceholderFormat(sq.Dollar)

type ItemStorage struct {
	pool *pgxpool.Pool
}

func (i *ItemStorage) GetAll(ctx context.Context, VariantID int) ([]entities.Item, error) {
	query, args, err := builder.Select("content", "uuid").From("item_table").Where(sq.Eq{"variant_id": VariantID}).ToSql()
	if err != nil {
		return nil, err
	}

	var Items []entities.Item
	rows, err := i.pool.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		var Item entities.Item
		err := rows.Scan(&Item.Content, &Item.UUID)
		if err != nil {
			return nil, err
		}
		Items = append(Items, Item)
	}

	return Items, nil

}

func NewItemStorage(pool *pgxpool.Pool) *ItemStorage {
	return &ItemStorage{pool: pool}
}

func (i *ItemStorage) Create(ctx context.Context, data map[string]interface{}) error {
	query, args, err := builder.Insert("item_table").SetMap(data).ToSql()
	if err != nil {
		return err
	}

	_, err = i.pool.Exec(ctx, query, args...)
	if err != nil {
		return err
	}
	return nil
}

func (i *ItemStorage) Update(ctx context.Context, updatedItem, ItemUUID string, VariantID int) error {
	query, args, err := builder.Update("item_table").
		Where(sq.Eq{"uuid": ItemUUID, "variant_id": VariantID}).
		Set("content", updatedItem).ToSql()
	if err != nil {
		return err
	}

	_, err = i.pool.Exec(ctx, query, args...)
	if err != nil {
		return err
	}
	return nil
}

func (i *ItemStorage) Delete(ctx context.Context, ItemUUID string, VariantID int) error {
	query, args, err := builder.Delete("item_table").Where(sq.Eq{"uuid": ItemUUID, "variant_id": VariantID}).ToSql()
	if err != nil {
		return err
	}

	_, err = i.pool.Exec(ctx, query, args...)
	if err != nil {
		return err
	}
	return nil
}
