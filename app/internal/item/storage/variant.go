package storage

import (
	"context"
	sq "github.com/Masterminds/squirrel"
	"github.com/jackc/pgx/v5/pgxpool"
)

type VariantStorage struct {
	pool *pgxpool.Pool
}

func NewVariantStorage(pool *pgxpool.Pool) *VariantStorage {
	return &VariantStorage{pool: pool}
}

func (v *VariantStorage) CheckExist(ctx context.Context, VariantID, ProductID int) bool {
	query, args, err := sq.Select("created_at").
		From("variant").Where(sq.Eq{"id": VariantID, "product_id": ProductID}).
		Prefix("SELECT exists(").Suffix(")").PlaceholderFormat(sq.Dollar).ToSql()
	if err != nil {
		return false
	}
	var exist bool
	row := v.pool.QueryRow(ctx, query, args...)
	err = row.Scan(&exist)
	if err != nil {
		return false
	}
	return exist

}
