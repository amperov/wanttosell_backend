package storage

import (
	"context"
	sq "github.com/Masterminds/squirrel"
	"github.com/jackc/pgx/v5/pgxpool"
)

type ProductStorage struct {
	pool *pgxpool.Pool
}

func NewProductStorage(pool *pgxpool.Pool) *ProductStorage {
	return &ProductStorage{pool: pool}
}

func (v *ProductStorage) CheckExist(ctx context.Context, ProductID, UserID int) bool {
	query, args, err := sq.Select("created_at").
		From("product").Where(sq.Eq{"id": ProductID, "user_id": UserID}).
		Prefix("SELECT exists(").Suffix(")").PlaceholderFormat(sq.Dollar).ToSql()
	if err != nil {
		return false
	}
	var exist bool
	row := v.pool.QueryRow(ctx, query, args...)
	err = row.Scan(&exist)
	if err != nil {
		return false
	}
	return exist

}
