package item

import (
	"app/internal/item/handlers"
	"app/internal/item/service"
	"app/internal/item/storage"
	"app/pkg/security/middleware"
	"github.com/gin-gonic/gin"
	"github.com/jackc/pgx/v5/pgxpool"
)

func RegisterItemService(ware middleware.MiddleWare, group *gin.RouterGroup, pool *pgxpool.Pool) {
	itemStorage := storage.NewItemStorage(pool)
	productStorage := storage.NewProductStorage(pool)
	variantStorage := storage.NewVariantStorage(pool)

	itemService := service.NewItemService(productStorage, itemStorage, variantStorage)

	itemHandlers := handlers.NewItemHandlers(itemService, ware)
	itemHandlers.Register(group)
}
