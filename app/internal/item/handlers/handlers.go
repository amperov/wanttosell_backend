package handlers

import (
	"app/internal/entities"
	"app/internal/item/handlers/inputs"
	"app/pkg/security"
	"app/pkg/security/middleware"
	"context"
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"io"
	"log"
	"strconv"
)

type ItemService interface {
	CreateOne(ctx context.Context, data map[string]interface{}) (string, error)
	UpdateOne(ctx context.Context, updatedItem, ItemUUID string, VariantID, ProductID, UserID int) error
	DeleteOne(ctx context.Context, ItemUUID string, VariantID, ProductID, UserID int) error

	GetAll(ctx context.Context, VariantID, ProductID, UserID int) ([]entities.Item, error)
}

type ItemHandlers struct {
	ItemService ItemService
	ware        middleware.MiddleWare
}

func NewItemHandlers(itemService ItemService, ware middleware.MiddleWare) *ItemHandlers {
	return &ItemHandlers{ItemService: itemService, ware: ware}
}

func (h *ItemHandlers) Register(rtr *gin.RouterGroup) {
	rtr.POST("/:product_id/variants/:variant_id/items/single", h.ware.Identity(h.ware.NeedRole(h.CreateItem, security.SellerAdminAccess)))
	rtr.POST("/:product_id/variants/:variant_id/items/multi", h.ware.Identity(h.ware.NeedRole(h.CreateItems, security.SellerAdminAccess)))

	rtr.GET("/:product_id/variants/:variant_id/items/", h.ware.Identity(h.ware.NeedRole(h.GetAllItems, security.SellerAdminAccess)))

	rtr.PATCH("/:product_id/variants/:variant_id/items/:item_uuid", h.ware.Identity(h.ware.NeedRole(h.UpdateItem, security.SellerAdminAccess)))

	rtr.DELETE("/:product_id/variants/:variant_id/items/single/:item_uuid", h.ware.Identity(h.ware.NeedRole(h.DeleteItem, security.SellerAdminAccess)))
	rtr.DELETE("/:product_id/variants/:variant_id/items/multi/", h.ware.Identity(h.ware.NeedRole(h.DeleteItems, security.SellerAdminAccess)))

	rtr.POST("/:product_id/variants/:variant_id/items/:item_uuid/complaint", h.ware.Identity(h.ware.NeedRole(h.MakeComplaint, security.SellerAdminAccess)))
}

// CreateItem - for seller. Adding single item per request
func (h *ItemHandlers) CreateItem(ctx *gin.Context) {
	var input inputs.CreateItemInput

	VarID := ctx.Param("variant_id")
	VariantID, err := strconv.Atoi(VarID)
	if err != nil {
		return
	}

	newUUID, err := uuid.NewUUID()
	ItemUUID := newUUID.String()

	BodyBytes, err := io.ReadAll(ctx.Request.Body)
	if err != nil {
		return
	}

	err = json.Unmarshal(BodyBytes, &input)
	if err != nil {
		return
	}

	input.VariantID = VariantID

	one, err := h.ItemService.CreateOne(context.Background(), input.ToMap(ItemUUID))
	if err != nil {
		return
	}

	ctx.JSON(201, fmt.Sprintf("%s : %s created", one, ItemUUID))
}

// CreateItems - for seller. Adding many items up to 1000
func (h *ItemHandlers) CreateItems(ctx *gin.Context) {

}

// GetAllItems - for seller. Show all added items.
func (h *ItemHandlers) GetAllItems(ctx *gin.Context) {
	VarID := ctx.Param("variant_id")
	VariantID, err := strconv.Atoi(VarID)
	if err != nil {
		return
	}

	ProdID := ctx.Param("product_id")
	ProductID, err := strconv.Atoi(ProdID)
	if err != nil {
		return
	}

	UserId, _ := ctx.Get("UserID")
	UserID := UserId.(int)

	all, err := h.ItemService.GetAll(context.Background(), VariantID, ProductID, UserID)
	if err != nil {
		log.Print(err)
		ctx.JSON(503, nil)
		return
	}
	ctx.JSON(200, struct {
		Items []entities.Item `json:"items"`
	}{
		Items: all,
	})
}

// UpdateItem - for seller. Correct added item by it`s uuid
func (h *ItemHandlers) UpdateItem(ctx *gin.Context) {
	var input inputs.UpdateItemInput

	BodyBytes, err := io.ReadAll(ctx.Request.Body)
	if err != nil {
		return
	}

	err = json.Unmarshal(BodyBytes, &input)
	if err != nil {
		return
	}

	ItemUUID := ctx.Param("item_uuid")

	VarID := ctx.Param("variant_id")
	VariantID, err := strconv.Atoi(VarID)
	if err != nil {
		return
	}

	ProdID := ctx.Param("product_id")
	ProductID, err := strconv.Atoi(ProdID)
	if err != nil {
		return
	}

	UserId, _ := ctx.Get("UserID")
	UserID := UserId.(int)

	err = h.ItemService.UpdateOne(context.Background(), input.Content, ItemUUID, VariantID, ProductID, UserID)
	if err != nil {
		return
	}
}

func (h *ItemHandlers) DeleteItem(ctx *gin.Context) {
	ItemUUID := ctx.Param("item_uuid")

	VarID := ctx.Param("variant_id")
	VariantID, err := strconv.Atoi(VarID)
	if err != nil {
		return
	}

	ProdID := ctx.Param("product_id")
	ProductID, err := strconv.Atoi(ProdID)
	if err != nil {
		return
	}

	UserId, _ := ctx.Get("UserID")
	UserID := UserId.(int)
	err = h.ItemService.DeleteOne(ctx, ItemUUID, VariantID, ProductID, UserID)
	if err != nil {
		return
	}
}

func (h *ItemHandlers) DeleteItems(ctx *gin.Context) {

}

func (h *ItemHandlers) MakeComplaint(ctx *gin.Context) {

}
