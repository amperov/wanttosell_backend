package inputs

type CreateItemInput struct {
	Content   string `json:"content"`
	UUID      string `json:"-"`
	VariantID int    `json:"-"`
}

func (i *CreateItemInput) ToMap(uuid string) map[string]interface{} {
	Map := make(map[string]interface{})
	Map["content"] = i.Content
	Map["uuid"] = uuid
	Map["variant_id"] = i.VariantID
	return Map
}
