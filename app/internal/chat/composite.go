package chat

import (
	"app/internal/chat/handlers"
	"app/pkg/security/middleware"
	"github.com/gin-gonic/gin"
)

func InitChatService(ware middleware.MiddleWare, group *gin.RouterGroup) {
	chatHandlers := handlers.NewChatHandlers(ware)

	chatHandlers.Register(group)
}
