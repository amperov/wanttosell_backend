package inputs

type ChatMessageInput struct {
	Message string `json:"message"`
	From    int    `json:"from"`
	To      int    `json:"to"`
}
