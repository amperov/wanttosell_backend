package handlers

import (
	"app/pkg/security/middleware"
	"context"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
	"log"
	"net/http"
	"time"
)

type ChatService interface {
	GetChat(ctx context.Context, UserID, ReceiverID int) *[]struct{} //TODO
}

type ChatHandlers struct {
	ware middleware.MiddleWare
}

func NewChatHandlers(ware middleware.MiddleWare) *ChatHandlers {
	return &ChatHandlers{ware: ware}
}

func (h *ChatHandlers) Register(group *gin.RouterGroup) {
	group.GET("/:user_id", h.ware.Identity(h.FetchChatByUserID))
	group.GET("/", h.ware.Identity(h.FetchAllChats))
}

type ChatMessageInput struct {
	Message string `json:"message"`
	From    int    `json:"from"`
	To      int    `json:"to"`
}

type Message struct {
	Text   string    `json:"text,omitempty"`
	SentAt time.Time `json:"sent_at,omitempty"`
}

type ChatSession struct {
	Conn     *websocket.Conn
	UserID   int
	messages []ChatMessageInput
}

var chatSessions = map[int]*ChatSession{}

func handleChatMessage(message ChatMessageInput) {
	// Add the message to the session's message list.
	for _, session := range chatSessions {
		if session.UserID == message.To {
			err := session.Conn.WriteJSON(Message{
				Text:   message.Message,
				SentAt: time.Now(),
			})
			if err != nil {
				log.Println(err)
			}
			// May be log to Database here
		}
	}
}

func (h *ChatHandlers) FetchChatByUserID(c *gin.Context) {
	// On frontend opening Chat
	// Handshake
	upgrader := websocket.Upgrader{
		CheckOrigin: func(r *http.Request) bool {
			return true
		},
	}

	conn, err := upgrader.Upgrade(c.Writer, c.Request, nil)
	if err != nil {
		log.Println(err)
		return
	}

	//We need search session with this pair IDs here!

	UserID, exists := c.Get("UserID")
	if !exists {
		c.JSON(402, nil)
	}

	if err != nil {
		return
	}

	var session = &ChatSession{}

	session = &ChatSession{
		Conn:   conn,
		UserID: UserID.(int),
	}
	chatSessions[UserID.(int)] = session

	// Add the chat session to the map of connected sessions.
	//Change method of storing session, because may be exist session with this pair IDs

	// Handle incoming chat messages.
	go func() {
		for {
			messageType, message, err := conn.ReadMessage()
			if err != nil {
				log.Println(err)
				break
			}

			if messageType != websocket.TextMessage {
				log.Println("Unexpected message type")
				break
			}

			// Decode the incoming message.
			var chatMessage ChatMessageInput
			err = json.Unmarshal(message, &chatMessage)
			if err != nil {
				log.Println(err)
				break
			}

			// Handle the chat message.
			handleChatMessage(chatMessage)
		}

		// Remove the chat session from the map of connected sessions.
		delete(chatSessions, UserID.(int))
	}()
}

func (h *ChatHandlers) FetchAllChats(ctx *gin.Context) {

}
