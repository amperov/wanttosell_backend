package services

import (
	"app/internal/authorization/controllers"
	"app/internal/authorization/controllers/inputs"
	"app/pkg/security/tokenManager"
	"app/pkg/tools"
	"context"
	"log"
)

type AuthStorage interface {
	CreateUser(ctx context.Context, Username, Firstname, Lastname, PasswordHash, Email string) (int, error)
	AuthUser(ctx context.Context, Login, PasswordHash string) (int, string, error)
}

type AuthService struct {
	manager     tokenManager.TokenManager
	AuthStorage AuthStorage
}

func NewAuthService(manager tokenManager.TokenManager, authStorage AuthStorage) *AuthService {
	return &AuthService{manager: manager, AuthStorage: authStorage}
}

func (s *AuthService) SignUp(ctx context.Context, Data *inputs.SignUpInput) (*controllers.AuthResponse, error) {
	UserID, err := s.AuthStorage.CreateUser(ctx, Data.Username, Data.Firstname, Data.Lastname, tools.HashPassword(Data.Password), Data.Email)
	if err != nil {
		return nil, err
	}

	AccessToken, RefreshToken, err := s.manager.GenerateTokens(UserID, "user")
	if err != nil {
		return nil, err
	}

	return &controllers.AuthResponse{AccessToken: AccessToken, RefreshToken: RefreshToken}, nil
}

func (s *AuthService) SignIn(ctx context.Context, Data *inputs.SignInInput) (*controllers.AuthResponse, error) {
	log.Print(tools.HashPassword(Data.Password))
	UserID, UserRole, err := s.AuthStorage.AuthUser(ctx, Data.Login, tools.HashPassword(Data.Password))
	if err != nil {
		return nil, err
	}

	AccessToken, RefreshToken, err := s.manager.GenerateTokens(UserID, UserRole)
	if err != nil {
		return nil, err
	}

	return &controllers.AuthResponse{AccessToken: AccessToken, RefreshToken: RefreshToken}, nil
}

func (s *AuthService) RefreshTokens(ctx context.Context, RefreshToken string) (*controllers.AuthResponse, error) {
	UserID, UserRole, err := s.manager.ValidateToken(RefreshToken)
	if err != nil {
		return nil, err
	}

	AccessToken, RefreshToken, err := s.manager.GenerateTokens(UserID, UserRole)
	if err != nil {
		return nil, err
	}
	return &controllers.AuthResponse{AccessToken: AccessToken, RefreshToken: RefreshToken}, nil
}
