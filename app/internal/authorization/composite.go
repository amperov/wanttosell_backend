package authorization

import (
	"app/internal/authorization/controllers"
	"app/internal/authorization/services"
	"app/internal/authorization/storages"
	"app/pkg/security/tokenManager"
	"github.com/gin-gonic/gin"
	"github.com/jackc/pgx/v5/pgxpool"
)

func RegisterAuthService(pool *pgxpool.Pool, manager tokenManager.TokenManager, group *gin.RouterGroup) {
	authStorage := storages.NewAuthStorage(pool)
	authService := services.NewAuthService(manager, authStorage)
	authHandlers := controllers.NewAuthHandlers(authService)
	authHandlers.Register(group)

}
