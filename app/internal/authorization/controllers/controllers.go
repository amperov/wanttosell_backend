package controllers

import (
	"app/internal/authorization/controllers/inputs"
	"context"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"io"
	"log"
)

type AuthResponse struct {
	AccessToken  string `json:"access_token,omitempty"`
	RefreshToken string `json:"refresh_token,omitempty"`
	Error        error  `json:"error,omitempty"`
}

type AuthService interface {
	SignUp(ctx context.Context, Data *inputs.SignUpInput) (*AuthResponse, error)
	SignIn(ctx context.Context, Data *inputs.SignInInput) (*AuthResponse, error)
	RefreshTokens(ctx context.Context, RefreshToken string) (*AuthResponse, error)
}

type AuthHandlers struct {
	AuthService AuthService
}

func NewAuthHandlers(authService AuthService) *AuthHandlers {
	return &AuthHandlers{AuthService: authService}
}

func (h *AuthHandlers) Register(router *gin.RouterGroup) {
	router.POST("/sign-up", h.RegisterUser)
	router.POST("/sign-in", h.AuthorizeUser)
	router.POST("/refresh", h.RefreshTokens)
}

func (h *AuthHandlers) RegisterUser(c *gin.Context) {
	var SignUp inputs.SignUpInput

	BodyBytes, err := io.ReadAll(c.Request.Body)
	if err != nil {
		log.Print(err)
		c.JSON(502, err)
		return
	}

	err = json.Unmarshal(BodyBytes, &SignUp)
	if err != nil {
		log.Print(err)
		c.JSON(502, err)
		return
	}

	AuthResponse, err := h.AuthService.SignUp(context.Background(), &SignUp)
	if err != nil {
		log.Print(err)
		return
	}
	c.JSON(201, AuthResponse)
}

func (h *AuthHandlers) AuthorizeUser(c *gin.Context) {
	var SignIn inputs.SignInInput

	BodyBytes, err := io.ReadAll(c.Request.Body)
	if err != nil {
		log.Print(err)
		c.JSON(502, err)
		return
	}

	err = json.Unmarshal(BodyBytes, &SignIn)
	if err != nil {
		c.JSON(502, err)
		return
	}

	AuthResponse, err := h.AuthService.SignIn(context.Background(), &SignIn)
	if err != nil {
		c.JSON(502, err)
		log.Print(err)
		return
	}
	c.JSON(200, AuthResponse)
}

func (h *AuthHandlers) RefreshTokens(c *gin.Context) {
	var Refresh inputs.RefreshTokenInput

	BodyBytes, err := io.ReadAll(c.Request.Body)
	if err != nil {
		return
	}

	err = json.Unmarshal(BodyBytes, &Refresh)
	if err != nil {
		return
	}

	AuthResponse, err := h.AuthService.RefreshTokens(context.Background(), Refresh.RefreshToken)
	if err != nil {
		return
	}
	c.JSON(200, AuthResponse)
}
