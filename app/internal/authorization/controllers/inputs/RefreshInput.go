package inputs

type RefreshTokenInput struct {
	RefreshToken string `json:"refresh_token"`
}
