package storages

import (
	"context"
	sq "github.com/Masterminds/squirrel"
	"github.com/jackc/pgx/v5/pgxpool"
	"log"
)

const UserTable = "user_table"

type AuthStorage struct {
	pool *pgxpool.Pool
}

func NewAuthStorage(pool *pgxpool.Pool) *AuthStorage {
	return &AuthStorage{pool: pool}
}

func (s *AuthStorage) CreateUser(ctx context.Context, Username, FirstName, Lastname, PasswordHash, Email string) (int, error) {
	var UserID int

	query, args, err := sq.Insert(UserTable).
		Columns("username", "firstname", "lastname", "email", "password_hash", "role").
		Values(Username, FirstName, Lastname, Email, PasswordHash, "user").
		PlaceholderFormat(sq.Dollar).Suffix("RETURNING id").ToSql()

	if err != nil {
		return 0, err
	}

	err = s.pool.QueryRow(ctx, query, args...).Scan(&UserID)
	if err != nil {
		return 0, err
	}

	return UserID, err
}

func (s *AuthStorage) AuthUser(ctx context.Context, Login, PasswordHash string) (int, string, error) {
	var UserID int
	var UserRole string
	query, args, err := sq.Select("id", "role").From(UserTable).Where(sq.Or{
		sq.Eq{"username": Login, "password_hash": PasswordHash},
		sq.Eq{"email": Login, "password_hash": PasswordHash}}).
		PlaceholderFormat(sq.Dollar).ToSql()
	if err != nil {
		return 0, "", err
	}

	err = s.pool.QueryRow(ctx, query, args...).Scan(&UserID, &UserRole)
	if err != nil {
		log.Print(err)
		return 0, "", err
	}
	return UserID, UserRole, nil
}
