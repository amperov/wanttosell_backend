package locales

import (
	"context"
	sq "github.com/Masterminds/squirrel"
	"github.com/jackc/pgx/v5/pgxpool"
)

const LocaleTable = "locale"

type LocaleStorage struct {
	pool *pgxpool.Pool
}

func NewLocaleStorage(pool *pgxpool.Pool) *LocaleStorage {
	return &LocaleStorage{pool: pool}
}

func (l *LocaleStorage) CreateOne(ctx context.Context, locale Locale) (int, error) {
	var id int
	query, args, err := sq.Insert(LocaleTable).Columns("lang", "value").Values(locale.Lang, locale.Value).PlaceholderFormat(sq.Dollar).Suffix("RETURNING id").ToSql()
	if err != nil {
		return 0, err
	}

	err = l.pool.QueryRow(ctx, query, args...).Scan(&id)
	if err != nil {
		return 0, err
	}

	return id, nil
}

func (l *LocaleStorage) GetAll(ctx context.Context, IDs []int) ([]Locale, error) {
	var Locales []Locale

	for _, id := range IDs {
		var Locale Locale
		Locale.ID = id

		query, args, err := sq.Select("lang", "value").From(LocaleTable).Where(sq.Eq{"id": id}).PlaceholderFormat(sq.Dollar).ToSql()
		if err != nil {
			return nil, err
		}

		err = l.pool.QueryRow(ctx, query, args...).Scan(&Locale.Lang, &Locale.Value)
		if err != nil {
			return nil, err
		}
		Locales = append(Locales, Locale)
	}

	return Locales, nil
}

func (l *LocaleStorage) UpdateOne(ctx context.Context, Locale Locale, ID int) error {
	query, args, err := sq.Update(LocaleTable).Where(sq.Eq{"id": ID, "lang": Locale.Lang}).Set("value", Locale.Value).PlaceholderFormat(sq.Dollar).ToSql()
	if err != nil {
		return err
	}

	_, err = l.pool.Exec(ctx, query, args...)
	if err != nil {
		return err
	}
	return nil
}

func (l *LocaleStorage) DeleteOne(ctx context.Context, ID int) error {
	query, args, err := sq.Delete(LocaleTable).Where(sq.Eq{"id": ID}).PlaceholderFormat(sq.Dollar).ToSql()
	if err != nil {
		return err
	}
	_, err = l.pool.Exec(ctx, query, args...)
	if err != nil {
		return err
	}
	return nil
}
