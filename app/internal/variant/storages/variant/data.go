package variant

import "time"

type Data struct {
	ID        int
	Title     []int
	Price     float64
	MinCount  int
	Available bool
	ProductID int
	CreatedAt time.Time
}
