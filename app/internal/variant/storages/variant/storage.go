package variant

import (
	"context"
	sq "github.com/Masterminds/squirrel"
	"github.com/jackc/pgx/v5/pgxpool"
)

type Storage struct {
	pool *pgxpool.Pool
}

func NewVariantStorage(pool *pgxpool.Pool) *Storage {
	return &Storage{pool: pool}
}

const VariantTable = "variant"

func (s *Storage) Create(ctx context.Context, Data map[string]interface{}) (int, error) {
	var VariantID int

	query, args, err := sq.Insert(VariantTable).Suffix("RETURNING id").SetMap(Data).PlaceholderFormat(sq.Dollar).ToSql()
	if err != nil {
		return 0, err
	}

	err = s.pool.QueryRow(ctx, query, args...).Scan(&VariantID)
	if err != nil {
		return 0, err
	}
	return VariantID, nil
}

func (s *Storage) Update(ctx context.Context, Data map[string]interface{}, VariantID, ProductID int) error {
	query, args, err := sq.Update(VariantTable).Where(sq.Eq{"id": VariantID, "product_id": ProductID}).PlaceholderFormat(sq.Dollar).SetMap(Data).ToSql()
	if err != nil {
		return err
	}

	_, err = s.pool.Exec(ctx, query, args...)
	if err != nil {
		return err
	}

	return nil
}

func (s *Storage) Delete(ctx context.Context, VariantID, ProductID int) error {
	query, args, err := sq.Delete(VariantTable).Where(sq.Eq{"id": VariantID, "product_id": ProductID}).PlaceholderFormat(sq.Dollar).ToSql()
	if err != nil {
		return err
	}

	_, err = s.pool.Exec(ctx, query, args...)
	if err != nil {
		return err
	}

	return nil
}

func (s *Storage) GetOne(ctx context.Context, VariantID int) (Data, error) {
	var Variant Data
	query, args, err := sq.Select("product_id", "title", "price", "min_count", "available", "created_at").
		Where(sq.Eq{"id": VariantID}).PlaceholderFormat(sq.Dollar).From(VariantTable).ToSql()
	if err != nil {
		return Data{}, err
	}

	err = s.pool.QueryRow(ctx, query, args...).Scan(
		&Variant.ProductID, &Variant.Title, &Variant.Price,
		&Variant.MinCount, &Variant.Available, &Variant.CreatedAt)
	if err != nil {
		return Data{}, err
	}

	return Variant, nil
}

func (s *Storage) GetAll(ctx context.Context, ProductID int) ([]Data, error) {
	var Variants []Data
	query, args, err := sq.Select("id", "title", "price", "min_count", "available", "created_at").
		Where(sq.Eq{"product_id": ProductID}).PlaceholderFormat(sq.Dollar).From(VariantTable).ToSql()
	if err != nil {
		return nil, err
	}

	rows, err := s.pool.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		var Variant Data
		err := rows.Scan(&Variant.ID, &Variant.Title, &Variant.Price,
			&Variant.MinCount, &Variant.Available, &Variant.CreatedAt)
		if err != nil {
			return nil, err
		}
		Variants = append(Variants, Variant)
	}

	return Variants, nil
}

func (s *Storage) Calculate() {
	//TODO implement me
	panic("implement me")
}
