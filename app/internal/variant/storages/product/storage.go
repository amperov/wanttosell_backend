package product

import (
	"context"
	sq "github.com/Masterminds/squirrel"
	"github.com/jackc/pgx/v5/pgxpool"
)

type Storage struct {
	pool *pgxpool.Pool
}

func NewProductStorage(pool *pgxpool.Pool) *Storage {
	return &Storage{pool: pool}
}

func (s *Storage) BelongToSeller(ctx context.Context, ProductID, UserID int) bool {
	var isExist bool
	query, args, err := sq.Select("id").From("product").
		Where(sq.Eq{"id": ProductID, "user_id": UserID}).
		Prefix("SELECT EXISTS(").Suffix(")").
		PlaceholderFormat(sq.Dollar).ToSql()
	if err != nil {
		return false
	}

	err = s.pool.QueryRow(ctx, query, args...).Scan(&isExist)
	if err != nil {
		return false
	}
	return isExist
}
