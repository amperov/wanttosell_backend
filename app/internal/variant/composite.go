package variant

import (
	"app/internal/variant/handlers"
	"app/internal/variant/services"
	"app/internal/variant/storages/locales"
	"app/internal/variant/storages/product"
	"app/internal/variant/storages/variant"
	"app/pkg/security/middleware"
	"github.com/gin-gonic/gin"
	"github.com/jackc/pgx/v5/pgxpool"
)

func RegisterVariantService(pool *pgxpool.Pool, ware middleware.MiddleWare, group *gin.RouterGroup) {
	VariantStorage := variant.NewVariantStorage(pool)
	ProductStorage := product.NewProductStorage(pool)
	LocaleStorage := locales.NewLocaleStorage(pool)

	VariantService := services.NewVariantService(VariantStorage, LocaleStorage, ProductStorage)

	VariantHandlers := handlers.NewVariantHandlers(ware, VariantService)

	VariantHandlers.Register(group)
}
