package handlers

import (
	"app/internal/entities"
	"app/internal/variant/handlers/inputs"
	"app/pkg/security"
	"app/pkg/security/middleware"
	"context"
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"io"
	"strconv"
)

type VariantHandlers struct {
	ware           middleware.MiddleWare
	VariantService VariantService
}

func NewVariantHandlers(ware middleware.MiddleWare, variantService VariantService) *VariantHandlers {
	return &VariantHandlers{ware: ware, VariantService: variantService}
}

type VariantService interface {
	CreateVariant(ctx context.Context, input *inputs.CreateVariantInput) (int, error)
	UpdateVariant(ctx context.Context, input *inputs.UpdateVariantInput, VariantID, ProductID, UserID int) error
	DeleteVariant(ctx context.Context, VariantID, ProductID, UserID int) error
	GetSellerVariantByID(ctx context.Context, VariantID, ProductID, UserID int) (*entities.SellerVariant, error)

	CalculatePrice(ctx context.Context, Count, VariantID, CustomerID int) (float64, error)
	GetAllVariants(ctx context.Context, ProductID int) ([]entities.ClientVariant, error)
}

func (h *VariantHandlers) Register(rtr *gin.RouterGroup) {
	rtr.POST("/:product_id/variants", h.ware.Identity(h.ware.NeedRole(h.CreateVariant, security.SellerAdminAccess)))
	rtr.PUT("/:product_id/variants/:variant_id", h.ware.Identity(h.ware.NeedRole(h.UpdateVariant, security.SellerAdminAccess)))
	rtr.DELETE("/:product_id/variants/:variant_id", h.ware.Identity(h.ware.NeedRole(h.DeleteVariant, security.SellerAdminAccess)))

	rtr.GET("/:product_id/variants/:variant_id/price", h.ware.Identity(h.CalculatePrice))
	rtr.GET("/:product_id/variants/:variant_id", h.ware.Identity(h.ware.NeedRole(h.GetVariantByID, security.SellerAdminAccess)))
	rtr.GET("/:product_id/variants", h.GetAllVariants)
	rtr.GET("/my/:product_id/variants", h.ware.Identity(h.ware.NeedRole(h.GetVariantsForSeller, security.SellerAdminAccess)))
}

func (h *VariantHandlers) CreateVariant(ctx *gin.Context) {
	var Input inputs.CreateVariantInput
	Userid, _ := ctx.Get("UserID")

	Input.CreatorID = Userid.(int)
	ProdId := ctx.Param("product_id")

	BodyBytes, err := io.ReadAll(ctx.Request.Body)
	if err != nil {
		return
	}

	Input.ProductID, err = strconv.Atoi(ProdId)

	err = json.Unmarshal(BodyBytes, &Input)
	if err != nil {
		return
	}

	VariantID, err := h.VariantService.CreateVariant(ctx, &Input)
	if err != nil {
		return
	}

	ctx.JSON(201, struct {
		Status  string `json:"status"`
		Message string `json:"message"`
	}{
		Status:  "success",
		Message: fmt.Sprintf("variant created with %d", VariantID),
	})
}

func (h *VariantHandlers) UpdateVariant(ctx *gin.Context) {
	var Input inputs.UpdateVariantInput

	Userid, _ := ctx.Get("UserID")
	ProdId := ctx.Param("product_id")
	VarId := ctx.Param("variant_id")

	ProductID, err := strconv.Atoi(ProdId)
	if err != nil {
		return
	}

	VariantID, err := strconv.Atoi(VarId)
	if err != nil {
		return
	}

	BodyBytes, err := io.ReadAll(ctx.Request.Body)
	if err != nil {
		return
	}

	err = json.Unmarshal(BodyBytes, &Input)
	if err != nil {
		return
	}

	err = h.VariantService.UpdateVariant(ctx, &Input, VariantID, ProductID, Userid.(int))
	if err != nil {
		return
	}

	ctx.JSON(201, struct {
		Status  string `json:"status"`
		Message string `json:"message"`
	}{
		Status:  "success",
		Message: fmt.Sprintf("variant updated with %d", VariantID),
	})
}

func (h *VariantHandlers) DeleteVariant(ctx *gin.Context) {
	Userid, _ := ctx.Get("UserID")
	ProdId := ctx.Param("product_id")
	VarId := ctx.Param("variant_id")

	ProductID, err := strconv.Atoi(ProdId)
	if err != nil {
		return
	}

	VariantID, err := strconv.Atoi(VarId)
	if err != nil {
		return
	}

	err = h.VariantService.DeleteVariant(ctx, VariantID, ProductID, Userid.(int))
	if err != nil {
		return
	}

	ctx.JSON(200, struct {
		Status  string `json:"status"`
		Message string `json:"message"`
	}{
		Status:  "success",
		Message: fmt.Sprintf("variant deleted with %d", VariantID),
	})
}

func (h *VariantHandlers) GetAllVariants(ctx *gin.Context) {
	ProdId := ctx.Param("product_id")
	ProductID, err := strconv.Atoi(ProdId)
	if err != nil {
		return
	}

	Variants, err := h.VariantService.GetAllVariants(context.Background(), ProductID)
	if err != nil {
		return
	}

	ctx.JSON(200, Variants)
}

func (h *VariantHandlers) GetVariantsForSeller(ctx *gin.Context) {
	ctx.Get("UserID")
	ProdId := ctx.Param("product_id")

	ProductID, err := strconv.Atoi(ProdId)
	if err != nil {
		return
	}

	Variants, err := h.VariantService.GetAllVariants(ctx, ProductID)
	if err != nil {
		return
	}

	ctx.JSON(200, Variants)
}

func (h *VariantHandlers) CalculatePrice(ctx *gin.Context) {
	count := ctx.Query("count")
	Count, err := strconv.Atoi(count)
	if err != nil {
		return
	}

	VarId := ctx.Param("variant_id")
	VariantID, err := strconv.Atoi(VarId)
	if err != nil {
		return
	}

	IsAuthed, _ := ctx.Get("Authed")
	if IsAuthed.(bool) {
		UserId, _ := ctx.Get("UserID")

		Price, err := h.VariantService.CalculatePrice(context.Background(), Count, VariantID, UserId.(int))
		if err != nil {
			return
		}

		ctx.JSON(200, struct {
			Count int     `json:"count"`
			Price float64 `json:"price"`
		}{
			Count: Count,
			Price: Price,
		})
	} else {
		Price, err := h.VariantService.CalculatePrice(context.Background(), Count, VariantID, 0)
		if err != nil {
			return
		}
		ctx.JSON(200, struct {
			Count int     `json:"count"`
			Price float64 `json:"price"`
		}{
			Count: Count,
			Price: Price,
		})
	}
}

func (h *VariantHandlers) GetVariantByID(ctx *gin.Context) {
	ProdId := ctx.Param("product_id")
	ProductID, err := strconv.Atoi(ProdId)
	if err != nil {
		return
	}

	VarId := ctx.Param("variant_id")
	VariantID, err := strconv.Atoi(VarId)
	if err != nil {
		return
	}

	Userid, _ := ctx.Get("UserID")

	Variant, err := h.VariantService.GetSellerVariantByID(context.Background(), VariantID, ProductID, Userid.(int))
	if err != nil {
		return
	}
	ctx.JSON(200, Variant)
}
