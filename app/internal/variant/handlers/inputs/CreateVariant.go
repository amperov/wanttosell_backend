package inputs

import (
	locales "app/internal/variant/storages/locales"
	"time"
)

type CreateVariantInput struct {
	Title     []locales.Locale `json:"title,omitempty"`
	Price     float64          `json:"price"`
	ProductID int              `json:"-"`
	CreatorID int              `json:"-"`
	Available bool             `json:"available"`
	MinCount  uint             `json:"min_count"`
}

func (i *CreateVariantInput) ToMap(TitleIDs []int) map[string]interface{} {
	Map := make(map[string]interface{})
	Map["title"] = TitleIDs
	Map["price"] = i.Price
	Map["available"] = i.Available
	Map["product_id"] = i.ProductID
	Map["min_count"] = i.MinCount
	Map["created_at"] = time.Now()
	return Map
}
