package inputs

import locales "app/internal/variant/storages/locales"

type UpdateVariantInput struct {
	Title    []locales.Locale `json:"title,omitempty"`
	Price    float64          `json:"price,omitempty"`
	MinCount uint             `json:"min_count,omitempty"`
}

func (i *UpdateVariantInput) ToMap() map[string]interface{} {
	Map := make(map[string]interface{})

	if i.Price != 0 {
		Map["price"] = i.Price
	}
	if i.MinCount != 0 {
		Map["min_count"] = i.MinCount
	}
	return Map
}
