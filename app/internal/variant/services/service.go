package services

import (
	"app/internal/entities"
	"app/internal/variant/handlers/inputs"
	locales "app/internal/variant/storages/locales"
	"app/internal/variant/storages/variant"
	"context"
	"fmt"
)

type VariantStorage interface {
	Create(ctx context.Context, Data map[string]interface{}) (int, error)
	Update(ctx context.Context, Data map[string]interface{}, VariantID, ProductID int) error
	Delete(ctx context.Context, VariantID, ProductID int) error

	Calculate()
	GetAll(ctx context.Context, ProductID int) ([]variant.Data, error)
	GetOne(ctx context.Context, VariantID int) (variant.Data, error)
}
type LocaleStorage interface {
	CreateOne(ctx context.Context, locale locales.Locale) (int, error)
	GetAll(ctx context.Context, IDs []int) ([]locales.Locale, error)
	UpdateOne(ctx context.Context, Locale locales.Locale, ID int) error
	DeleteOne(ctx context.Context, ID int) error
}

type ProductStorage interface {
	BelongToSeller(ctx context.Context, ProductID, UserID int) bool
}
type VariantService struct {
	Variant VariantStorage
	Locale  LocaleStorage
	Product ProductStorage
}

func NewVariantService(variant VariantStorage, locale LocaleStorage, product ProductStorage) *VariantService {
	return &VariantService{Variant: variant, Locale: locale, Product: product}
}

func (s *VariantService) CreateVariant(ctx context.Context, input *inputs.CreateVariantInput) (int, error) {
	var TitleIDs []int

	IsBelong := s.Product.BelongToSeller(ctx, input.ProductID, input.CreatorID)
	if !IsBelong {
		return 0, fmt.Errorf("you dont have access to product [%d]", input.ProductID)
	}

	for _, locale := range input.Title {
		LocaleID, err := s.Locale.CreateOne(ctx, locale)
		if err != nil {
			return 0, err
		}
		TitleIDs = append(TitleIDs, LocaleID)
	}

	VariantID, err := s.Variant.Create(ctx, input.ToMap(TitleIDs))
	if err != nil {
		return 0, err
	}

	return VariantID, nil
}

func (s *VariantService) UpdateVariant(ctx context.Context, input *inputs.UpdateVariantInput, VariantID, ProductID, UserID int) error {

	IsBelong := s.Product.BelongToSeller(ctx, ProductID, UserID)
	if !IsBelong {
		return fmt.Errorf("you dont have access to product [%d]", ProductID)
	}

	if input.Title != nil {
		VariantData, err := s.Variant.GetOne(ctx, VariantID)
		if err != nil {
			return err
		}

		Titles, err := s.Locale.GetAll(ctx, VariantData.Title)
		if err != nil {
			return err
		}

		for _, title := range Titles {
			for _, locale := range input.Title {
				if title.Lang == locale.Lang {
					err := s.Locale.UpdateOne(ctx, locale, title.ID)
					if err != nil {
						return err
					}
				}
			}
		}
	}
	err := s.Variant.Update(ctx, input.ToMap(), VariantID, ProductID)
	if err != nil {
		return err
	}
	return nil
}

func (s *VariantService) DeleteVariant(ctx context.Context, VariantID, ProductID, UserID int) error {

	IsBelong := s.Product.BelongToSeller(ctx, ProductID, UserID)
	if !IsBelong {
		return fmt.Errorf("you dont have access to product [%d]", ProductID)
	}

	Variant, err := s.Variant.GetOne(ctx, VariantID)
	if err != nil {
		return err
	}

	for _, id := range Variant.Title {
		err := s.Locale.DeleteOne(ctx, id)
		if err != nil {
			return err
		}
	}

	err = s.Variant.Delete(ctx, VariantID, ProductID)
	if err != nil {
		return err
	}
	return nil
}

func (s *VariantService) GetAllVariants(ctx context.Context, ProductID int) ([]entities.ClientVariant, error) {
	VariantDatas, err := s.Variant.GetAll(ctx, ProductID)
	if err != nil {
		return nil, err
	}

	var Variants []entities.ClientVariant

	for i := 0; i < len(VariantDatas); i++ {
		var Variant entities.ClientVariant

		Variant.Title, err = s.Locale.GetAll(ctx, VariantDatas[i].Title)
		if err != nil {
			return nil, err
		}

		Variant.ID = VariantDatas[i].ID
		Variant.Price = VariantDatas[i].Price
		Variant.MinCount = uint(VariantDatas[i].MinCount)
		// TODO MaxCount - in ItemStorage
		Variant.Available = VariantDatas[i].Available

		Variants = append(Variants, Variant)
	}

	return Variants, nil
}

func (s *VariantService) GetSellerVariantByID(ctx context.Context, VariantID, ProductID, UserID int) (*entities.SellerVariant, error) {
	Belong := s.Product.BelongToSeller(ctx, ProductID, UserID)

	if !Belong {
		return nil, fmt.Errorf("you dont have permissions")
	}

	Variant, err := s.Variant.GetOne(ctx, VariantID)
	if err != nil {
		return nil, err
	}

	TitleVariant, err := s.Locale.GetAll(ctx, Variant.Title)
	if err != nil {
		return nil, err
	}

	return &entities.SellerVariant{
		Title:     TitleVariant,
		Price:     Variant.Price,
		Available: Variant.Available,
		MinCount:  uint(Variant.MinCount),
		MaxCount:  0,
		CreatedAt: Variant.CreatedAt,
	}, nil
}

func (s *VariantService) CalculatePrice(ctx context.Context, Count, VariantID, CustomerID int) (float64, error) {
	//TODO implement me
	panic("implement me")
}
