package entities

import (
	"app/internal/variant/storages/locales"
	"time"
)

// ClientVariant - only in arrays for Clients on FullProduct's page
type ClientVariant struct {
	ID        int              `json:"id"`
	Title     []locales.Locale `json:"title"`
	Price     float64          `json:"price"`
	MaxCount  uint             `json:"max_count"` // How many seller uploaded positions in Variant?
	MinCount  uint             `json:"min_count"` // From which count customer can buy this Variant?
	Available bool             `json:"available"` // Can customer buy variant?
}

// SellerVariant needed only for seller because for Customer -- needed only list available variants
type SellerVariant struct {
	Title     []locales.Locale `json:"title"`
	Price     float64          `json:"price"`
	Available bool             `json:"available"`
	MinCount  uint             `json:"min_count"` //just Setting for Customer
	MaxCount  uint             `json:"max_count"` //How many keys have seller? = CountKeys - KeysStorage only, not VariantStorage
	CreatedAt time.Time        `json:"-"`
}
