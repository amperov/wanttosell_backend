package entities

import (
	"app/internal/product/storages/locales"
	"time"
)

type FullProduct struct {
	ID          int              `json:"id"`
	Title       []locales.Locale `json:"title"`
	Photo       string           `json:"photo"`
	Type        string           `json:"type"`
	Description []locales.Locale `json:"description"`
	Delivery    string           `json:"delivery"`
	Platform    string           `json:"platform"`
	OS          string           `json:"os"`
	SellerID    int              `json:"seller_id"`
}

type MiniProduct struct {
	ID    int              `json:"id"`
	Title []locales.Locale `json:"title"`
	Photo string           `json:"photo"`
}

type SellerMiniProduct struct {
	ID          int              `json:"id"`
	Title       []locales.Locale `json:"title"`
	Type        string           `json:"type"`
	Description []locales.Locale `json:"description"`
	Photo       string           `json:"photo"`
	Delivery    string           `json:"delivery"`
	Platform    string           `json:"platform"`
	OS          string           `json:"os"`
	SellerID    int              `json:"seller_id"`
	CreatedAt   time.Time        `json:"created_at"`
}
