package entities

type UserEntity struct {
	ID           uint   `json:"id"`
	Firstname    string `json:"firstname"`
	Lastname     string `json:"lastname"`
	Username     string `json:"username"`
	Role         string `json:"role"`
	Email        string `json:"email"`
	PasswordHash string `json:"password_hash"`
}
