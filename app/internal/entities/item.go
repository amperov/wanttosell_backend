package entities

type Item struct {
	Content string `json:"content,omitempty"`
	UUID    string `json:"uuid,omitempty"`
}
