package product

import (
	"app/internal/product/handlers"
	"app/internal/product/services"
	"app/internal/product/storages/images"
	"app/internal/product/storages/locales"
	"app/internal/product/storages/product"
	"app/pkg/security/middleware"
	"github.com/gin-gonic/gin"
	"github.com/jackc/pgx/v5/pgxpool"
)

func RegisterProductService(pool *pgxpool.Pool, ware *middleware.MiddleWare, client services.S3_Client, group *gin.RouterGroup) {
	imagesStorage := images.NewImagesStorage(pool)
	LocaleStorage := locales.NewLocaleStorage(pool)
	productStorage := product.NewProductStorage(pool)
	productService := services.NewProductService(productStorage, client, imagesStorage, LocaleStorage)
	productHandlers := handlers.NewProductHandlers(ware, productService)
	productHandlers.Register(group)
}
