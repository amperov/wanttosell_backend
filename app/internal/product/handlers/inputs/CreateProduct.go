package inputs

import (
	"app/internal/product/storages/locales"
	"time"
)

type CreateProductInput struct {
	Title       []locales.Locale `json:"title"`
	Description []locales.Locale `json:"description"`
	OS          string           `json:"os"`       // Windows, iOS etc
	Delivery    string           `json:"delivery"` // Auto Delivery, Preorder etc
	Platform    string           `json:"platform"` // Steam, EGS, Battle.net etc
	Type        string           `json:"type"`     // Key activation, Account etc
	CreatorID   int              `json:"-"`        // userID who created this product
	CreatedAt   time.Time        `json:"-"`
}

func (i *CreateProductInput) ToMap(Title, Descriptions []int) map[string]interface{} {
	Map := make(map[string]interface{})
	Map["title"] = Title
	Map["os"] = i.OS
	Map["delivery"] = i.Delivery
	Map["product_type"] = i.Type
	Map["description"] = Descriptions
	Map["platform"] = i.Platform
	Map["user_id"] = i.CreatorID
	Map["created_at"] = time.Now()
	return Map
}
