package inputs

import "app/internal/product/storages/locales"

type UpdateProductInput struct {
	Title       []locales.Locale `json:"title"`
	Description []locales.Locale `json:"description"`
	OS          string           `json:"os"`       // Windows, iOS etc
	Delivery    string           `json:"delivery"` // Auto Delivery, Preorder etc
	Region      string           `json:"region"`   // Turkey
	Platform    string           `json:"platform"` // Steam, EGS, Battle.net etc
	Type        string           `json:"type"`     // Key activation, Account etc
}

func (i *UpdateProductInput) ToMap() map[string]interface{} {
	Map := make(map[string]interface{})
	if i.OS != "" {
		Map["os"] = i.OS
	}
	if i.Region != "" {
		Map["region"] = i.Region
	}
	if i.Delivery != "" {
		Map["delivery"] = i.Delivery
	}
	if i.Type != "" {
		Map["product_type"] = i.Type
	}
	if i.Platform != "" {
		Map["platform"] = i.Platform
	}

	return Map
}
