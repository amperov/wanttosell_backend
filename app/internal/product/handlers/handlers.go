package handlers

import (
	"app/internal/entities"
	"app/internal/product/handlers/filter"
	"app/internal/product/handlers/inputs"
	"app/internal/product/storages/images/data"
	"app/pkg/responses"
	"app/pkg/security"
	"app/pkg/security/middleware"
	"context"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"io"
	"log"
	"net/http"
	"strconv"
)

type ProductService interface {
	GetProducts(ctx context.Context, Filters filter.Filter) (*[]entities.MiniProduct, error)
	GetProductByID(ctx context.Context, ProductID int) (*entities.FullProduct, error)
	GetProductsBySellerID(ctx context.Context, SellerID int) (*[]entities.SellerMiniProduct, error)
	GetPhotos(ctx context.Context, ProductID int) (*data.ProductPhotos, error)

	UploadPhoto(ctx context.Context, ProductID int, Type string, File ...io.ReadSeeker) ([]string, error)
	CreateProduct(ctx context.Context, input *inputs.CreateProductInput) (int, error)
	UpdateProduct(ctx context.Context, Input *inputs.UpdateProductInput, ProductID, UserID int) error
	DeleteProduct(ctx context.Context, ProductID, UserID int) error
}

type ProductHandlers struct {
	mw             *middleware.MiddleWare
	ProductService ProductService
}

func NewProductHandlers(mw *middleware.MiddleWare, productService ProductService) *ProductHandlers {
	return &ProductHandlers{mw: mw, ProductService: productService}
}

func (h *ProductHandlers) Register(rtr *gin.RouterGroup) {
	rtr.GET("/", h.GetAllProducts)
	rtr.GET("/my", h.mw.Identity(h.mw.NeedRole(h.GetOwnProducts, security.SellerAdminAccess)))
	rtr.GET("/:product_id", h.mw.Identity(h.mw.NeedRole(h.GetProduct, security.SellerAdminAccess)))

	rtr.GET("/:product_id/photos", h.mw.Identity(h.mw.NeedRole(h.GetPhotos, security.SellerAdminAccess)))
	rtr.POST("/:product_id/upload", h.mw.Identity(h.mw.NeedRole(h.UploadPhotos, security.SellerAdminAccess)))

	rtr.POST("/", h.mw.Identity(h.mw.NeedRole(h.CreateProduct, security.SellerAdminAccess)))
	rtr.PATCH("/:product_id", h.mw.Identity(h.mw.NeedRole(h.UpdateProduct, security.SellerAdminAccess)))
	rtr.DELETE("/:product_id", h.mw.Identity(h.mw.NeedRole(h.DeleteProduct, security.SellerAdminAccess)))
}

//Client SubService

func (h *ProductHandlers) GetAllProducts(ctx *gin.Context) {
	var Filter filter.Filter
	WithRegion := ctx.Query("region")
	if WithRegion != "" {
		Filter.Region = WithRegion
	}
	WithOs := ctx.Query("os")
	if WithOs != "" {
		Filter.OS = WithOs
	}
	WithPlatform := ctx.Query("platform")
	if WithPlatform != "" {
		Filter.Platform = WithPlatform
	}
	WithProductType := ctx.Query("product_type")
	if WithPlatform != "" {
		Filter.Type = WithProductType
	}

	Products, err := h.ProductService.GetProducts(context.Background(), Filter)
	if err != nil {
		ctx.JSON(500, err)
		return
	}
	ctx.JSON(200, struct {
		Products *[]entities.MiniProduct `json:"products"`
	}{
		Products,
	})
}

func (h *ProductHandlers) GetProduct(ctx *gin.Context) {
	param := ctx.Param("product_id")
	ProductID, err := strconv.Atoi(param)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, err)
		return
	}
	log.Print(ProductID)
	FullProduct, err := h.ProductService.GetProductByID(context.Background(), ProductID)
	if err != nil {
		ctx.JSON(500, err)
		return
	}

	ctx.JSON(200, FullProduct)
}

//Seller SubService

func (h *ProductHandlers) CreateProduct(ctx *gin.Context) {
	var Input inputs.CreateProductInput
	UserId, _ := ctx.Get("UserID")
	Input.CreatorID = UserId.(int)

	BodyBytes, err := io.ReadAll(ctx.Request.Body)
	if err != nil {
		log.Println(err)
		ctx.JSON(500, err)
		return
	}

	err = json.Unmarshal(BodyBytes, &Input)
	if err != nil {
		log.Println(err)
		ctx.JSON(500, err)
		return
	}

	ProductID, err := h.ProductService.CreateProduct(context.Background(), &Input)
	if err != nil {
		log.Println(err)
		ctx.JSON(500, err)
		return
	}

	ctx.JSON(201, struct {
		StatusMessage string `json:"status_message"`
		CreatedID     int    `json:"id"`
	}{
		StatusMessage: "success",
		CreatedID:     ProductID,
	})
}

func (h *ProductHandlers) UpdateProduct(ctx *gin.Context) {
	var Input inputs.UpdateProductInput

	UserId, _ := ctx.Get("UserID")

	param := ctx.Param("product_id")
	ProductID, err := strconv.Atoi(param)
	if err != nil {
		ctx.JSON(500, err)
		return
	}

	BodyBytes, err := io.ReadAll(ctx.Request.Body)
	if err != nil {
		ctx.JSON(500, err)
		return
	}

	err = json.Unmarshal(BodyBytes, &Input)
	if err != nil {
		ctx.JSON(500, err)
		return
	}

	err = h.ProductService.UpdateProduct(context.Background(), &Input, ProductID, UserId.(int))
	if err != nil {
		ctx.JSON(500, err)
		return
	}

	ctx.JSON(201, struct {
		StatusMessage string `json:"status_message"`
		UpdatedID     int    `json:"id"`
	}{
		StatusMessage: "success",
		UpdatedID:     ProductID,
	})
}

func (h *ProductHandlers) DeleteProduct(ctx *gin.Context) {
	UserId, _ := ctx.Get("UserID")

	param := ctx.Param("product_id")
	ProductID, err := strconv.Atoi(param)
	if err != nil {
		log.Println(err)
		ctx.JSON(http.StatusInternalServerError, err)
		return
	}

	err = h.ProductService.DeleteProduct(context.Background(), ProductID, UserId.(int))
	if err != nil {
		log.Println(err)
		ctx.JSON(502, err)
		return
	}

	ctx.JSON(200, nil)
}

func (h *ProductHandlers) GetOwnProducts(ctx *gin.Context) {
	UserId, _ := ctx.Get("UserID")

	Products, err := h.ProductService.GetProductsBySellerID(context.Background(), UserId.(int))
	if err != nil {
		ctx.JSON(500, nil)
		return
	}
	ctx.JSON(200, Products)
}

func (h *ProductHandlers) UploadPhotos(ctx *gin.Context) {

	ProductID, err := strconv.Atoi(ctx.Param("product_id"))
	if err != nil {
		ctx.JSON(402, err)
		log.Print(err.Error())
		return
	}

	file, err := ctx.FormFile("file")
	if err != nil {
		ctx.JSON(402, err)
		return
	}

	mainPhoto, err := file.Open()
	if err != nil {
		ctx.JSON(402, err)
		return
	}

	_, err = h.ProductService.UploadPhoto(context.Background(), ProductID, "main", mainPhoto)
	if err != nil {
		ctx.JSON(402, err)
		return
	}
	/*var Files []io.ReadSeeker
	HasAdditional := true
	for i := 1; i < 4 && HasAdditional; i++ {
		addFile, err := ctx.FormFile(fmt.Sprintf("additional_%d", i))
		if err != nil {
			HasAdditional = false
			continue
		}
		NewFile, err := addFile.Open()
		if err != nil {
			HasAdditional = false
			continue
		}
		Files = append(Files, NewFile)
	}*/
	/*_, err = h.ProductService.UploadPhoto(context.Background(), ProductID, "additional", Files...)
	if err != nil {
		ctx.JSON(402, err)
		log.Print(err)
		return
	}*/
	ctx.JSON(201, responses.SuccessResponse{StatusMessage: "success"})
}

func (h *ProductHandlers) GetPhotos(ctx *gin.Context) {
	ProductID, err := strconv.Atoi(ctx.Param("product_id"))
	if err != nil {
		ctx.JSON(502, nil)
	}

	photos, err := h.ProductService.GetPhotos(ctx, ProductID)
	if err != nil {
		ctx.JSON(500, nil)
		return
	}

	ctx.JSON(200, photos)
}
