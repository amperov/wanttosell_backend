package filter

type Filter struct {
	Region, OS, Platform, Type string
}
