package locales

type Locale struct {
	ID    int    `json:"-"`
	Lang  string `json:"lang"`
	Value string `json:"value"`
}
