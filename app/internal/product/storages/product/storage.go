package product

import (
	"context"
	sq "github.com/Masterminds/squirrel"
	"github.com/jackc/pgx/v5/pgxpool"
	"log"
)

const ProductTable = "product"

type ProductStorage struct {
	pool *pgxpool.Pool
}

func NewProductStorage(pool *pgxpool.Pool) *ProductStorage {
	return &ProductStorage{pool: pool}
}

func (s *ProductStorage) CreateProduct(ctx context.Context, Data map[string]interface{}) (int, error) {
	var ProductID int
	query, args, err := sq.Insert(ProductTable).SetMap(Data).PlaceholderFormat(sq.Dollar).Suffix("RETURNING id").ToSql()
	if err != nil {
		return 0, err
	}

	err = s.pool.QueryRow(ctx, query, args...).Scan(&ProductID)
	if err != nil {
		return 0, err
	}
	return ProductID, nil
}

func (s *ProductStorage) UpdateProduct(ctx context.Context, ProductID int, Data map[string]interface{}, UserID int) error {
	query, args, err := sq.Update(ProductTable).SetMap(Data).PlaceholderFormat(sq.Dollar).Where(sq.Eq{"id": ProductID, "user_id": UserID}).ToSql()
	if err != nil {
		return err
	}

	_, err = s.pool.Exec(ctx, query, args...)
	if err != nil {
		return err
	}
	return nil
}

func (s *ProductStorage) DeleteProduct(ctx context.Context, ProductID, UserID int) error {
	query, args, err := sq.Delete(ProductTable).PlaceholderFormat(sq.Dollar).Where(sq.Eq{"id": ProductID, "user_id": UserID}).ToSql()
	if err != nil {
		return err
	}

	_, err = s.pool.Exec(ctx, query, args...)
	if err != nil {
		return err
	}
	return nil
}

func (s *ProductStorage) GetProductByID(ctx context.Context, ProductID int) (*FullProductData, error) {
	var Product FullProductData
	query, args, err := sq.Select("id", "title", "user_id", "delivery", "product_type", "description", "os", "platform").
		Where(sq.Eq{"id": ProductID}).From(ProductTable).PlaceholderFormat(sq.Dollar).ToSql()
	if err != nil {
		return nil, err
	}

	row := s.pool.QueryRow(ctx, query, args...)
	err = row.Scan(&Product.ID, &Product.Title, &Product.SellerID, &Product.Delivery, &Product.Type,
		&Product.Description, &Product.OS, &Product.Platform)
	if err != nil {
		log.Print(err)
		return nil, err
	}

	return &Product, nil
}

func (s *ProductStorage) GetProductsBySellerID(ctx context.Context, SellerID int) ([]SellerMiniProductData, error) {
	var Products []SellerMiniProductData
	query, args, err := sq.Select("id", "title", "description", "platform", "os", "product_type", "delivery", "created_at").
		Where(sq.Eq{"user_id": SellerID}).From(ProductTable).PlaceholderFormat(sq.Dollar).ToSql()
	if err != nil {
		return nil, err
	}

	rows, err := s.pool.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		var Product SellerMiniProductData
		err := rows.Scan(&Product.ID, &Product.Title,
			&Product.Description, &Product.Platform,
			&Product.OS, &Product.Type,
			&Product.Delivery, &Product.CreatedAt)
		if err != nil {
			continue
		}
		Products = append(Products, Product)
	}

	return Products, nil
}

func (s *ProductStorage) GetProducts(ctx context.Context) ([]MiniProductData, error) {
	var Products []MiniProductData
	query, args, err := sq.Select("id", "title").From(ProductTable).PlaceholderFormat(sq.Dollar).ToSql()
	if err != nil {
		return nil, err
	}

	rows, err := s.pool.Query(ctx, query, args...)
	if err != nil {
		log.Print(err)
		return nil, err
	}
	for rows.Next() {
		var Product MiniProductData
		err := rows.Scan(&Product.ID, &Product.Title)
		if err != nil {
			log.Print(err)
		}

		Products = append(Products, Product)
	}
	return Products, nil
}
