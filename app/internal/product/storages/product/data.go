package product

import "time"

type MiniProductData struct {
	ID    int
	Title []int
	Photo string
}

type FullProductData struct {
	ID          int
	Title       []int
	Type        string
	Description []int
	Delivery    string
	Platform    string
	OS          string
	SellerID    int
}

type SellerMiniProductData struct {
	ID          int
	Title       []int
	Type        string
	Description []int
	Delivery    string
	Platform    string
	OS          string
	SellerID    int
	CreatedAt   time.Time
}
