package images

import (
	"context"
	sq "github.com/Masterminds/squirrel"
	"github.com/jackc/pgx/v5/pgxpool"
)

const ImagesTable = "images_table"

type ImagesStorage struct {
	pool *pgxpool.Pool
}

func NewImagesStorage(pool *pgxpool.Pool) *ImagesStorage {
	return &ImagesStorage{pool: pool}
}

func (i *ImagesStorage) GetAdditionalPhoto(ctx context.Context, ProductID int) ([]string, error) {
	var FileKey []string
	query, args, err := sq.Select("file_key").From(ImagesTable).Where(sq.Eq{"product_id": ProductID, "type": "additional"}).PlaceholderFormat(sq.Dollar).ToSql()
	if err != nil {
		return nil, err
	}

	rows, err := i.pool.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		var Key string
		err := rows.Scan(&Key)
		if err != nil {
			return nil, err
		}
		FileKey = append(FileKey, Key)
	}

	return FileKey, nil
}

func (i *ImagesStorage) UploadPhoto(ctx context.Context, ProductID int, Type, FileKey string) error {
	query, args, err := sq.Insert(ImagesTable).Columns("product_id", "type", "file_key").Values(ProductID, Type, FileKey).PlaceholderFormat(sq.Dollar).ToSql()
	if err != nil {
		return err
	}
	_, err = i.pool.Exec(ctx, query, args...)
	if err != nil {
		return err
	}

	return nil
}

func (i *ImagesStorage) GetMainPhoto(ctx context.Context, ProductID int) (string, error) {
	var FileKey string
	query, args, err := sq.Select("file_key").From(ImagesTable).Where(sq.Eq{"product_id": ProductID, "type": "main"}).PlaceholderFormat(sq.Dollar).ToSql()
	if err != nil {
		return "", err
	}

	err = i.pool.QueryRow(ctx, query, args...).Scan(&FileKey)
	if err != nil {
		return "", err
	}
	return FileKey, nil

}
