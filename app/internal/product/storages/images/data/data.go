package data

type ProductPhotos struct {
	MainPhoto        string   `json:"main_photo,omitempty"`
	AdditionalPhotos []string `json:"additional_photos,omitempty"`
}
