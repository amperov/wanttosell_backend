package services

import (
	"app/internal/entities"
	"app/internal/product/handlers/filter"
	"app/internal/product/handlers/inputs"
	"app/internal/product/storages/images/data"
	"app/internal/product/storages/locales"
	"app/internal/product/storages/product"
	"context"
	"fmt"
	"github.com/google/uuid"
	"io"
	"log"
)

type LocaleStorage interface {
	CreateOne(ctx context.Context, locale locales.Locale) (int, error)
	GetAll(ctx context.Context, IDs []int) ([]locales.Locale, error)
	UpdateOne(ctx context.Context, Locale locales.Locale, ID int) error
	DeleteOne(ctx context.Context, ID int) error
}

type ImageStorage interface {
	UploadPhoto(ctx context.Context, ProductID int, Type, FileKey string) error
	GetAdditionalPhoto(ctx context.Context, ProductID int) ([]string, error)
	GetMainPhoto(ctx context.Context, ProductID int) (string, error)
}

type S3_Client interface {
	SendFile(ctx context.Context, FileKey string, File io.ReadSeeker) error
	GetFile(ctx context.Context, FileKey string) (string, error)
}

type ProductStorage interface {
	CreateProduct(ctx context.Context, Data map[string]interface{}) (int, error)
	UpdateProduct(ctx context.Context, ProductID int, Data map[string]interface{}, UserID int) error
	DeleteProduct(ctx context.Context, ProductID int, UserID int) error

	GetProductByID(ctx context.Context, ProductID int) (*product.FullProductData, error)
	GetProductsBySellerID(ctx context.Context, SellerID int) ([]product.SellerMiniProductData, error)
	GetProducts(ctx context.Context) ([]product.MiniProductData, error)
}

type ProductService struct {
	s3             S3_Client
	ProductStorage ProductStorage
	Image          ImageStorage
	LocaleStorage  LocaleStorage
}

func NewProductService(productStorage ProductStorage, client S3_Client, storage ImageStorage, localeStorage LocaleStorage) *ProductService {
	return &ProductService{ProductStorage: productStorage, s3: client, Image: storage, LocaleStorage: localeStorage}
}

func (s *ProductService) GetPhotos(ctx context.Context, ProductID int) (*data.ProductPhotos, error) {
	MainPhoto, err := s.Image.GetMainPhoto(ctx, ProductID)
	if err != nil {
		return nil, err
	}

	MainLink, err := s.s3.GetFile(ctx, MainPhoto)
	if err != nil {
		log.Printf("From S3: %s\n", err.Error())
		return nil, err
	}

	AdditionalPhotos, err := s.Image.GetAdditionalPhoto(ctx, ProductID)
	if err != nil {
		return nil, err
	}

	var Resp = data.ProductPhotos{
		MainPhoto:        MainLink,
		AdditionalPhotos: nil,
	}

	for _, photo := range AdditionalPhotos {
		Link, err := s.s3.GetFile(ctx, photo)
		if err != nil {
			return nil, err
		}

		Resp.AdditionalPhotos = append(Resp.AdditionalPhotos, Link)
	}

	return &Resp, nil
}

func (s *ProductService) UploadPhoto(ctx context.Context, ProductID int, Type string, Files ...io.ReadSeeker) ([]string, error) {

	var UUIDs []string
	for _, file := range Files {
		UUID := uuid.New().String()

		FileKey := fmt.Sprintf("%s_%s_%d.png", UUID, Type, ProductID)
		err := s.s3.SendFile(ctx, FileKey, file)
		if err != nil {
			log.Print(err)
			return nil, err
		}

		//Set photo in DB
		err = s.Image.UploadPhoto(ctx, ProductID, Type, FileKey)
		if err != nil {
			log.Print(err)
			return nil, err
		}
		UUIDs = append(UUIDs, UUID)
	}

	return UUIDs, nil
}

func (s *ProductService) GetProducts(ctx context.Context, Filters filter.Filter) (*[]entities.MiniProduct, error) {
	ProductsData, err := s.ProductStorage.GetProducts(ctx)
	if err != nil {
		return nil, err
	}

	var ProductsResponse []entities.MiniProduct

	for i := 0; i < len(ProductsData); i++ {
		var Product entities.MiniProduct

		Product.Title, err = s.LocaleStorage.GetAll(ctx, ProductsData[i].Title)
		if err != nil {
			return nil, err
		}

		PhotoUUID, err := s.Image.GetMainPhoto(ctx, ProductsData[i].ID)
		if err != nil {
			continue
		}

		LinkToFile, err := s.s3.GetFile(ctx, PhotoUUID)
		if err != nil {
			return nil, err
		}

		Product.ID = ProductsData[i].ID
		Product.Photo = LinkToFile

		ProductsResponse = append(ProductsResponse, Product)
	}
	return &ProductsResponse, nil
}

func (s *ProductService) GetProductByID(ctx context.Context, ProductID int) (*entities.FullProduct, error) {

	Product, err := s.ProductStorage.GetProductByID(ctx, ProductID)
	if err != nil {
		return nil, err
	}

	ProductLocales, err := s.LocaleStorage.GetAll(ctx, Product.Title)
	if err != nil {
		return nil, err
	}

	ProductDescriptions, err := s.LocaleStorage.GetAll(ctx, Product.Description)
	if err != nil {
		return nil, err
	}

	photo, err := s.Image.GetMainPhoto(ctx, ProductID)
	LinkToImg, err := s.s3.GetFile(ctx, photo)
	if err != nil {
		return nil, err
	}

	return &entities.FullProduct{
		ID:          ProductID,
		Photo:       LinkToImg,
		Title:       ProductLocales,
		Type:        Product.Type,
		Description: ProductDescriptions,
		Delivery:    Product.Delivery,
		Platform:    Product.Platform,
		OS:          Product.OS,
		SellerID:    Product.SellerID,
	}, nil
}

func (s *ProductService) GetProductsBySellerID(ctx context.Context, SellerID int) (*[]entities.SellerMiniProduct, error) {
	ProductsData, err := s.ProductStorage.GetProductsBySellerID(ctx, SellerID)
	if err != nil {
		return nil, err
	}
	log.Println(ProductsData)
	var ProductsResponse []entities.SellerMiniProduct

	for i := 0; i < len(ProductsData); i++ {
		var Product entities.SellerMiniProduct

		Product.Title, err = s.LocaleStorage.GetAll(ctx, ProductsData[i].Title)
		if err != nil {
			return nil, err
		}
		Product.Description, err = s.LocaleStorage.GetAll(ctx, ProductsData[i].Description)

		PhotoUUID, err := s.Image.GetMainPhoto(ctx, ProductsData[i].ID)
		if err != nil {
			continue
		}

		LinkToFile, err := s.s3.GetFile(ctx, PhotoUUID)
		if err != nil {
			return nil, err
		}

		Product.ID = ProductsData[i].ID
		Product.Platform = ProductsData[i].Platform
		Product.CreatedAt = ProductsData[i].CreatedAt
		Product.OS = ProductsData[i].OS
		Product.Type = ProductsData[i].Type
		Product.Delivery = ProductsData[i].Delivery
		Product.Photo = LinkToFile
		Product.SellerID = SellerID
		ProductsResponse = append(ProductsResponse, Product)
	}
	return &ProductsResponse, nil
}

func (s *ProductService) CreateProduct(ctx context.Context, input *inputs.CreateProductInput) (int, error) {
	var Title, Descriptions []int
	for _, locale := range input.Title {
		LocaleID, err := s.LocaleStorage.CreateOne(ctx, locale)
		if err != nil {
			return 0, err
		}
		Title = append(Title, LocaleID)
	}
	for _, locale := range input.Description {
		LocaleID, err := s.LocaleStorage.CreateOne(ctx, locale)
		if err != nil {
			return 0, err
		}
		Descriptions = append(Descriptions, LocaleID)
	}

	ProductID, err := s.ProductStorage.CreateProduct(ctx, input.ToMap(Title, Descriptions))
	if err != nil {
		return 0, err
	}
	return ProductID, nil
}

func (s *ProductService) UpdateProduct(ctx context.Context, input *inputs.UpdateProductInput, ProductID, UserID int) error {
	ProductData, err := s.ProductStorage.GetProductByID(ctx, ProductID)
	if err != nil {
		return err
	}

	TitleLocales, err := s.LocaleStorage.GetAll(ctx, ProductData.Title)
	if err != nil {
		return err
	}
	DescriptionLocales, err := s.LocaleStorage.GetAll(ctx, ProductData.Description)
	if err != nil {
		return err
	}

	if input.Title != nil {
		for i := 0; i < len(TitleLocales); i++ {
			for j := 0; j < len(input.Title); j++ {
				if TitleLocales[i].Lang == input.Title[j].Lang {
					err := s.LocaleStorage.UpdateOne(ctx, input.Title[j], TitleLocales[i].ID)
					if err != nil {
						return err
					}
				}
			}
		}
	}
	if input.Description != nil {
		for i := 0; i < len(DescriptionLocales); i++ {
			for j := 0; j < len(input.Description); j++ {
				if DescriptionLocales[i].Lang == input.Description[j].Lang {
					err := s.LocaleStorage.UpdateOne(ctx, input.Description[j], DescriptionLocales[i].ID)
					if err != nil {
						return err
					}
				}
			}
		}
	}

	err = s.ProductStorage.UpdateProduct(ctx, ProductID, input.ToMap(), UserID)
	if err != nil {
		return err
	}
	return nil
}

func (s *ProductService) DeleteProduct(ctx context.Context, ProductID, UserID int) error {
	err := s.ProductStorage.DeleteProduct(ctx, ProductID, UserID)
	if err != nil {
		return err
	}
	return nil
}
