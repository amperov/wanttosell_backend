FROM golang:1.20-alpine AS builder

WORKDIR /usr/local/src

RUN apk --no-cache add bash git make gcc gettext musl-dev

#deps
COPY ["./app/go.mod", "./app/go.sum", "./"]
RUN go mod download
EXPOSE 8080
COPY app ./
RUN go build ./app.go

FROM alpine as runner

COPY --from=builder /usr/local/src/app /

COPY ./.env ./

EXPOSE 8080
CMD ["/app"]