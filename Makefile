SHELL := /bin/bash

PROD_DB_VERSION=14
PROD_DB_HOST=212.233.72.128
PROD_DB_USERNAME=Amperov
PROD_DB_PASSWORD=AmperovIsSad.2077
PROD_DB_NAME=WantToSell_Testing

DEV_DB_VERSION=14
DEV_DB_HOST=localhost
DEV_DB_PORT=5432
DEV_DB_USERNAME=Amperov
DEV_DB_PASSWORD=AmperovIsSad.2077
DEV_DB_NAME=WantToSell


local_db:dev-db-run migrate-up

dev-db-run:
	docker run --name=$(DB_NAME) \
	-e POSTGRES_USER=$(DB_USERNAME) \
	-e POSTGRES_PASSWORD=$(DB_PASSWORD) \
	-e POSTGRES_DB=$(DB_NAME) \
 	-p $(DB_PORT):$(DB_PORT) -d postgres:$(DB_VERSION)

migrate-up:
	migrate -path ./migrations -database "postgres://$(PROD_DB_USERNAME):$(PROD_DB_PASSWORD)@$(PROD_DB_HOST):$(PROD_DB_PORT)/$(PROD_DB_NAME)?sslmode=disable" up


migrate-down:
	migrate -path ./migrations -database "postgres://$(DB_USERNAME):$(DB_PASSWORD)@$(DB_HOST):$(DB_PORT)/$(DB_NAME)?sslmode=disable" down

dev-run:
	docker run --name=backend -p 8080:8080  theamperov/wanttosell_backend:dev &


prod-run:
	docker run --name=backend -p 8080:8080  theamperov/wanttosell_backend:prod &