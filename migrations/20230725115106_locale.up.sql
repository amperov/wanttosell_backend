CREATE TABLE locale(
    id serial primary key ,
    lang text not null ,
    value text not null
);