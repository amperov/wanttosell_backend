CREATE TABLE product(
    id serial primary key ,
    title int[] not null ,
    description int[] not null ,
    platform text not null ,
    os text not null ,
    product_type text not null ,
    delivery text not null ,
    user_id int,
    created_at timestamp
);

CREATE TABLE variant(
    id serial primary key ,
    title int[] not null ,
    min_count int not null,
    available bool not null ,
    price float not null ,
    created_at timestamp,
    product_id int references product (id) ON DELETE CASCADE
);