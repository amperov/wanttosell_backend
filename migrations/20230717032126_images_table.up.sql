CREATE TABLE images_table(
    id serial primary key ,
    file_key text,
    type varchar(15),
    product_id int
);