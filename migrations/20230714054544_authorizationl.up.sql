CREATE TABLE user_table(
    id serial primary key ,
    username text,
    firstname text,
    lastname text,
    role text,
    email text,
    password_hash text
);

INSERT INTO user_table (username, role, email, password_hash, firstname, lastname)
values ('amperov', 'admin', 'amperov.gopher@gmail.com', 'c5f41bd43de70874ee7aa28b24672946fc1859704006f4e3dea82e3dccd5f4b1','Александр', 'Амперов')

