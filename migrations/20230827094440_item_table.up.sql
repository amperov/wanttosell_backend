CREATE TABLE item_table(
    uuid text not null unique ,
    content text not null unique,
    variant_id int references variant (id) on delete cascade
);